<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Menu_model');
    }
    public function index()
    {
        $temp = $this->Menu_model->get();

        $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($temp));
    }

    public function getFavouriteOnly()
    {
        $data = $this->Menu_model->getFavourite();

        $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }

    public function favourite()
    {
        $id = $this->input->get('id');
        if ($id === null) {
            $this->output
            ->set_content_type('application/json')
            ->set_status_header(500)
            ->set_output(json_encode(array(
                "error" => "ID isnull"
            )));
        }
        $res = $this->Menu_model->toggleFavourite($id);
        $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode(array(
                "updated" => $res,
                "status" => true
            )));
    }
}