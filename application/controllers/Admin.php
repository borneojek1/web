<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Menu_model');
		$this->load->helper('url');
	}
	public function index()
	{
		$data["items"] = $this->Menu_model->get();
		$data["favourites"] = $this->Menu_model->getFavourite();
		$this->load->view('admin', $data);
	}

	public function form()
	{
		$id = $this->input->get("id");
		$title = $this->input->post("title");
		if ($title) {
			$data = [
				"title" => $this->input->post("title", true),
				"description" => $this->input->post("description", true),
				"image_url" => $this->input->post("image_url", true),
				"original_price" => $this->input->post("original_price", true),
				"final_price" => $this->input->post("final_price", true),
				"is_favourite" => $this->input->post("is_favourite", true) == "on",
			];
			if ($id) {
				$res = $this->Menu_model->update($id, $data);
			} else {
				$res = $this->Menu_model->add($data);
			}
			if ($res > 0) {
				redirect("admin");
			} else {
				$this->load->view('form', $data);	
			}
		} else {
			if ($id) {
				$data["isCreate"] = false;
				$data["item"] = $this->Menu_model->getByID($id);
			} else {
				$data["isCreate"] = true;
			}
			$this->load->view('form', $data);
		}
	}

	public function delete()
	{
		$id = $this->input->get("id");
		$this->Menu_model->delete($id);
		redirect("admin");
	}
}
