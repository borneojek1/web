<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Form</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
		text-decoration: none;
	}

	a:hover {
		color: #97310e;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
		min-height: 96px;
	}

	p {
		margin: 0 0 10px;
		padding:0;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}

	input[type=text], select, input[type=number] {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }

    input[type=submit] {
        background-color: #2a9d8f; /* Green */
        border: none;
        color: white;
        border-radius: 4px;
        padding: 6px 18px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 1.25em;
		font-weight: bold;
		margin: 4px 2px;
		cursor: pointer;
        margin-top: 16px;
    }

    input[type=submit]:hover {
        background-color: #268A7F;
    }

    form {
        max-width: 600px;
        margin-left: auto;
        margin-right: auto;
    }
	</style>
</head>
<body>

<div id="container">
	<h1>Form</h1>
	<div id="body">
		<form action="" method="post">
            <div class="group">
                <label>Title</label>
                <input type="text" name="title" value="<?= !$isCreate ? $item->title : '' ?>" />
            </div>
            <div class="group">
                <label>Description</label>
                <input type="text" name="description" value="<?= !$isCreate ? $item->description : '' ?>" />
            </div>
            <div class="group">
                <label>Image URL</label>
                <input type="text" name="image_url" value="<?= !$isCreate ? $item->image_url : '' ?>" />
            </div>
            <div class="group">
                <label>Original Price</label>
                <input type="number" name="original_price" value="<?= !$isCreate ? $item->original_price : '' ?>" />
            </div>
            <div class="group">
                <label>Final Price</label>
                <input type="number" name="final_price" value="<?= !$isCreate ? $item->final_price : '' ?>" />
            </div>
            <div class="group">
                <label>Is Favourite</label>
                <input type="checkbox" name="is_favourite" <?= !$isCreate ? $item->is_favourite == "1" ? 'checked' : '' : '' ?> />
            </div>
            <center>
                <input type="submit" value="Submit">
            </center>
        </form> 
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>
