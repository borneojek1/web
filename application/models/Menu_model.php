<?php

class Menu_model extends CI_Model {
    public function get() {
        return $this->db->get('tb_menu')->result_array();
    }

    public function getByID($id) {
        return $this->db->get_where('tb_menu', array('id' => $id))->first_row();
    }

    public function getFavourite() {
        return $this->db->get_where('tb_menu', array('is_favourite' => 1))->result_array();
    }

    public function toggleFavourite($id) {
        $this->db->set('is_favourite', '!is_favourite', false);
        $this->db->where('id', $id);
        $this->db->update('tb_menu');
        return $this->db->affected_rows();
    }

    public function add($data) {
        $this->db->insert("tb_menu", $data);
        return $this->db->affected_rows();
    }

    public function update($id, $data) {
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update('tb_menu');
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->delete('tb_menu', array('id' => $id));
        return $this->db->affected_rows();
    }
}