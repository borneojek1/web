<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Tb_menu extends CI_Migration {
    public function __construct() {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                    'type' => 'INT',
                    'constraint' => 5,
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
            ),
            'title' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
            ),
            'description' => array(
                    'type' => 'TEXT',
                    'null' => TRUE,
            ),
            'image_url' => array(
                'type' => 'TEXT',
                'null' => TRUE,
            ),
            'is_favourite' => array(
                'type' => 'BOOL',
                'null' => TRUE,
            ),
            'original_price' => array(
                'type' => 'DOUBLE',
                'null' => TRUE,
            ),
            'final_price' => array(
                'type' => 'DOUBLE',
                'null' => TRUE,
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('tb_menu');
    }

    public function down()
    {
        $this->dbforge->drop_table('tb_menu');
    }
}